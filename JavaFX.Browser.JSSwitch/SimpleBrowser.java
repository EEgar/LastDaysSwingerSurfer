import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.File;

public class SimpleBrowser extends Application {

    @Override
    public void start(Stage primaryStage) {
        // Створюємо головний контейнер
        BorderPane root = new BorderPane();

        // Створюємо TabPane для табів
        TabPane tabPane = new TabPane();

        // Додаємо обробник подій для подвійного кліку на TabPane
        tabPane.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                Tab newTab = createNewTab("https://www.example.com");
                tabPane.getTabs().add(newTab);
                tabPane.getSelectionModel().select(newTab);
            }
        });

        // Створюємо перший таб
        Tab tab1 = createNewTab("https://www.example.com");
        tabPane.getTabs().add(tab1);

        // Створюємо HBox для адресного рядка та кнопок
        HBox addressBar = new HBox();
        TextField urlField = new TextField("https://www.example.com");
        Button goButton = new Button("Go");
        Button newTabButton = new Button("New Tab");
        Button reloadButton = new Button("Reload");
        CheckBox jsCheckBox = new CheckBox("Enable JavaScript");
        jsCheckBox.setSelected(true); // Вихідний стан - JavaScript увімкнено

        // Додаємо обробник подій для кнопки "Go"
        goButton.setOnAction(event -> {
            String url = urlField.getText().trim();
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "https://" + url;
            }
            WebEngine webEngine = ((WebView) ((BorderPane) tabPane.getSelectionModel().getSelectedItem().getContent()).getCenter()).getEngine();
            webEngine.load(url);
        });

        // Додаємо обробник подій для кнопки "New Tab"
        newTabButton.setOnAction(event -> {
            Tab newTab = createNewTab("https://www.example.com");
            tabPane.getTabs().add(newTab);
            tabPane.getSelectionModel().select(newTab);
        });

        // Додаємо обробник подій для кнопки "Reload"
        reloadButton.setOnAction(event -> {
            WebEngine webEngine = ((WebView) ((BorderPane) tabPane.getSelectionModel().getSelectedItem().getContent()).getCenter()).getEngine();
            webEngine.reload();
        });

        // Додаємо обробник подій для CheckBox "Enable JavaScript"
        jsCheckBox.setOnAction(event -> {
            WebEngine webEngine = ((WebView) ((BorderPane) tabPane.getSelectionModel().getSelectedItem().getContent()).getCenter()).getEngine();
            webEngine.setJavaScriptEnabled(jsCheckBox.isSelected());
        });

        // Додаємо все до HBox
        addressBar.getChildren().addAll(urlField, goButton, newTabButton, reloadButton, jsCheckBox);

        // Розміщуємо addressBar у верхній частині BorderPane
        root.setTop(addressBar);
        root.setCenter(tabPane);

        // Створюємо сцену та задаємо її для primaryStage
        Scene scene = new Scene(root, 1024, 768);
        primaryStage.setTitle("Simple JavaFX Browser");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    // Метод для створення нового табу
    private Tab createNewTab(String url) {
        BorderPane tabContent = new BorderPane();
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load(url);

        // Apply brightness reduction effect
        ColorAdjust colorAdjust = new ColorAdjust();
        colorAdjust.setBrightness(-0.3); // Adjust brightness level as needed
        webView.setEffect(colorAdjust);

        // Додаємо обробник подій для перетягування та скидання файлів
        webView.setOnDragOver(event -> {
            if (event.getGestureSource() != webView && event.getDragboard().hasFiles()) {
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            event.consume();
        });

        webView.setOnDragDropped(event -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            if (db.hasFiles()) {
                File file = db.getFiles().get(0);
                webEngine.load(file.toURI().toString());
                success = true;
            }
            event.setDropCompleted(success);
            event.consume();
        });

        tabContent.setCenter(webView);

        Tab tab = new Tab("New Tab", tabContent);
        tab.setOnSelectionChanged(event -> {
            if (tab.isSelected()) {
                ((TextField) ((HBox) ((BorderPane) tab.getTabPane().getScene().getRoot()).getTop()).getChildren().get(0)).setText(webEngine.getLocation());
            }
        });

        return tab;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
