import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.*;
import java.awt.*;

public class WebBrowser {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            final JFrame frame = new JFrame();
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setSize(800, 600);

            final JFXPanel fxPanel = new JFXPanel();
            frame.add(fxPanel, BorderLayout.CENTER);

            Platform.runLater(() -> {
                BorderPane borderPane = new BorderPane();
                Scene scene = new Scene(borderPane);
                fxPanel.setScene(scene);

                TabPane tabPane = new TabPane();
                javafx.scene.control.Button newTabButton = new javafx.scene.control.Button("New Tab");
                newTabButton.setOnAction(e -> addNewTab(tabPane));

                // Adding new tab button to the top pane
                FlowPane topPane = new FlowPane();
                topPane.getChildren().add(newTabButton);
                borderPane.setTop(topPane);

                borderPane.setCenter(tabPane);

                // Set up the JFrame
                frame.setTitle("Web Browser");
                frame.setVisible(true);

                // Add the first tab
                addNewTab(tabPane);
            });
        });
    }

    private static void addNewTab(TabPane tabPane) {
        Tab tab = new Tab("New Tab");
        BorderPane tabContent = new BorderPane();
        tab.setContent(tabContent);

        WebView webView = new WebView();
        WebEngine engine = webView.getEngine();

        // Apply brightness reduction effect directly in Java code
        ColorAdjust colorAdjust = new ColorAdjust();
        colorAdjust.setBrightness(-0.3); // Adjust brightness level as needed
        webView.setEffect(colorAdjust);

        // Navigation buttons
        javafx.scene.control.Button backButton = new javafx.scene.control.Button("Back");
        backButton.setOnAction(e -> engine.getHistory().go(-1));
        javafx.scene.control.Button forwardButton = new javafx.scene.control.Button("Forward");
        forwardButton.setOnAction(e -> engine.getHistory().go(1));
        javafx.scene.control.Button reloadButton = new javafx.scene.control.Button("Reload");
        reloadButton.setOnAction(e -> engine.reload());

        // URL bar
        TextField urlField = new TextField();
        urlField.setOnAction(e -> {
            String url = urlField.getText();
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "https://" + url;
            }
            engine.load(url);
        });
        urlField.setText("https://duckduckgo.com/");

        // Adding components to the tab content
        FlowPane navigationPane = new FlowPane(backButton, forwardButton, reloadButton, urlField);
        tabContent.setTop(navigationPane);
        tabContent.setCenter(webView);

        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);

        // Load the initial URL
        engine.load(urlField.getText());
    }
}
