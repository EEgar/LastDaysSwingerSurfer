import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.*;
import java.awt.*;

public class WebBrowser {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            final JFrame frame = new JFrame();
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setSize(800, 600);

            final JFXPanel fxPanel = new JFXPanel();
            frame.add(fxPanel, BorderLayout.CENTER);

            Platform.runLater(() -> {
                BorderPane borderPane = new BorderPane();
                Scene scene = new Scene(borderPane);
                fxPanel.setScene(scene);

                WebView webView = new WebView();
                WebEngine engine = webView.getEngine();

                // Apply brightness reduction effect directly in Java code
                ColorAdjust colorAdjust = new ColorAdjust();
                colorAdjust.setBrightness(-0.3); // Adjust brightness level as needed
                webView.setEffect(colorAdjust);

                // Navigation buttons
                Button backButton = new Button("Back");
                backButton.setOnAction(e -> engine.getHistory().go(-1));
                Button forwardButton = new Button("Forward");
                forwardButton.setOnAction(e -> engine.getHistory().go(1));
                Button reloadButton = new Button("Reload");
                reloadButton.setOnAction(e -> engine.reload());

                // URL bar
                TextField urlField = new TextField();
                urlField.setOnAction(e -> engine.load(urlField.getText()));
                urlField.setText("https://duckduckgo.com/");

                // Adding components to the border pane
                borderPane.setTop(new FlowPane(backButton, forwardButton, reloadButton, urlField));
                borderPane.setCenter(webView);

                // Set up the JFrame
                frame.setTitle("Anarchy Web Browser");
                frame.setVisible(true);

                // Load the initial URL
                engine.load(urlField.getText());
            });
        });
    }
}

//javac --module-path /usr/lib/jvm/java-22-openjdk/lib --add-modules javafx.controls,javafx.fxml,javafx.swing,javafx.web,javafx.graphics WebBrowser.java
//java --module-path /usr/lib/jvm/java-22-openjdk/lib --add-modules javafx.controls,javafx.fxml,javafx.swing,javafx.web,javafx.graphics WebBrowser
