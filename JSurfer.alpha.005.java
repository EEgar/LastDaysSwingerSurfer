import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.SwingUtilities;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.StyleSheet;

public class JSurfer extends JFrame {

    private JTabbedPane tabbedPane = new JTabbedPane();
    private StatusBar statusBar = new StatusBar();
    private static final String ABOUT_BLANK = "blank";
    private boolean isNightMode = false;

    public JSurfer() {
        super("JSurfer Web Browser");

        JMenu fileMenu = new JMenu("File");
        fileMenu.add(new NewTabAction());
        fileMenu.addSeparator();
        fileMenu.add(new ExitAction());
        fileMenu.setMnemonic('F');
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);
        setJMenuBar(menuBar);

        JPanel contentPane = new JPanel(new BorderLayout());
        setContentPane(contentPane);

        contentPane.add(tabbedPane, BorderLayout.CENTER);
        contentPane.add(statusBar, BorderLayout.SOUTH);

        tabbedPane.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
                    createNewTab();
                }
            }
        });

        createNewTab();
    }

    private void createNewTab() {
        JPanel panel = new JPanel(new BorderLayout());
        WebBrowserPane webBrowserPane = new WebBrowserPane(this);
        webBrowserPane.goToURL(ABOUT_BLANK);

        webBrowserPane.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    statusBar.setMessage("Loading...");
                } else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
                    statusBar.setMessage(e.getURL().toString());
                } else if (e.getEventType() == HyperlinkEvent.EventType.EXITED) {
                    statusBar.setMessage(" ");
                }
            }
        });

        webBrowserPane.setDropTarget(new DropTarget(webBrowserPane, DnDConstants.ACTION_COPY, new DropTargetHandler(webBrowserPane)));

        WebToolBar toolBar = new WebToolBar(webBrowserPane, this);
        panel.add(toolBar, BorderLayout.NORTH);
        panel.add(new JScrollPane(webBrowserPane), BorderLayout.CENTER);
        tabbedPane.addTab("New Tab", panel);
        tabbedPane.setTabComponentAt(tabbedPane.getTabCount() - 1, new ButtonTabComponent(tabbedPane));

        // Apply dark mode to new tab if night mode is enabled
        if (isNightMode) {
            applyDarkMode(panel, true);
        }
    }

    public void applyDarkMode(Component component, boolean enable) {
        if (component instanceof JTabbedPane) {
            JTabbedPane tabbedPane = (JTabbedPane) component;
            for (int i = 0; i < tabbedPane.getTabCount(); i++) {
                Component tabComponent = tabbedPane.getComponentAt(i);
                applyDarkMode(tabComponent, enable);
            }
        } else if (component instanceof JPanel) {
            JPanel panel = (JPanel) component;
            for (Component child : panel.getComponents()) {
                applyDarkMode(child, enable);
            }
        }

        if (enable) {
            component.setBackground(new Color(34, 34, 34));
            component.setForeground(new Color(238, 238, 238));
        } else {
            component.setBackground(UIManager.getColor("Panel.background"));
            component.setForeground(UIManager.getColor("Panel.foreground"));
        }

        component.repaint();
    }

    private class NewTabAction extends AbstractAction {
        public NewTabAction() {
            putValue(Action.NAME, "New Browser Tab");
            putValue(Action.SHORT_DESCRIPTION, "Create New Web Browser Tab");
        }

        public void actionPerformed(ActionEvent event) {
            createNewTab();
        }
    }

    private class ExitAction extends AbstractAction {
        public ExitAction() {
            putValue(Action.NAME, "Exit");
            putValue(Action.SHORT_DESCRIPTION, "Exit Application");
        }

        public void actionPerformed(ActionEvent event) {
            System.exit(0);
        }
    }

    private class DropTargetHandler implements DropTargetListener {
        private WebBrowserPane webBrowserPane;

        public DropTargetHandler(WebBrowserPane webBrowserPane) {
            this.webBrowserPane = webBrowserPane;
        }

        public void drop(DropTargetDropEvent event) {
            Transferable transferable = event.getTransferable();
            if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                event.acceptDrop(DnDConstants.ACTION_COPY);
                try {
                    List<File> fileList = (List<File>) transferable.getTransferData(DataFlavor.javaFileListFlavor);
                    for (File file : fileList) {
                        String path = file.getAbsolutePath();
                        if (path.endsWith(".html") || path.endsWith(".txt") || path.endsWith(".htm") || file.length() < 1024 * 1024) {
                            webBrowserPane.goToURL(file.toURI().toURL());
                        }
                    }
                    event.dropComplete(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    event.dropComplete(false);
                }
            } else {
                event.rejectDrop();
            }
        }

        public void dragEnter(DropTargetDragEvent event) {
            if (event.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
                event.acceptDrag(DnDConstants.ACTION_COPY);
            else {
                event.rejectDrag();
            }
        }

        public void dragExit(DropTargetEvent event) {
        }

        public void dragOver(DropTargetDragEvent event) {
        }

        public void dropActionChanged(DropTargetDragEvent event) {
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JSurfer jsurfer = new JSurfer();
            jsurfer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jsurfer.setSize(800, 600);
            jsurfer.setVisible(true);
        });
    }
}

class WebBrowserPane extends JEditorPane {

    private List<URL> history = new ArrayList<>();
    private int historyIndex;
    private StyleSheet styleSheet;
    boolean nightMode = false; // Make this package-private
    private Color nightModeBackgroundColor = new Color(34, 34, 34);
    private Color nightModeForegroundColor = new Color(238, 238, 238);
    private JSurfer parent;

    public WebBrowserPane(JSurfer parent) {
        this.parent = parent;
        setEditable(false);
        setContentType("text/html");
        setPageBrightness(0.3); // Set the initial brightness to 30%
    }

    public void toggleNightMode() {
        nightMode = !nightMode;
        String nightModeCSS = "body { background-color: #222; color: #eee; }";
        String defaultCSS = "body { background-color: #fff; color: #000; }";
        
        if ("text/html".equals(getContentType())) {
            HTMLDocument doc = (HTMLDocument) getDocument();
            StyleSheet styles = doc.getStyleSheet();
            
            if (nightMode) {
                styles.addRule(nightModeCSS);
                setPageBrightness(0.3); // Set the brightness to 30% when night mode is on
            } else {
                styles.addRule(defaultCSS);
                setPageBrightness(1.0); // Set the brightness to 100% when night mode is off
            }
        }

        // Apply dark mode to the entire UI
        parent.applyDarkMode(parent.getContentPane(), nightMode);
    }

    public void goToURL(URL url) {
        displayPage(url);
        history.add(url);
        historyIndex = history.size() - 1;
    }

    public void setPageBrightness(double brightness) {
 
             String css = "body { filter: brightness(" + brightness + "); }";
        ((HTMLDocument) getDocument()).getStyleSheet().addRule(css);
    }

    public void goToURL(String urlString) {
        try {
            if (urlString.startsWith("file:")) {
                File file = new File(new URL(urlString).toURI());
                if (file.exists() && file.isFile() && file.length() < 1024 * 1024) {
                    String content = new String(java.nio.file.Files.readAllBytes(file.toPath()));
                    setText(content);
                    setContentType("text/plain");
                } else {
                    URL url = new URL(urlString);
                    goToURL(url);
                }
            } else {
                URL url = new URL(correctURLString(urlString));
                goToURL(url);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String correctURLString(String urlText) throws MalformedURLException {
        if (urlText == null || urlText.isEmpty()) {
            return null;
        }

        URL url;
        try {
            url = new URL(urlText);
        } catch (MalformedURLException e) {
            url = new URL("http://" + urlText);
        }

        return url.toString();
    }

    public URL forward() {
        if (historyIndex < history.size() - 1) {
            historyIndex++;
            return history.get(historyIndex);
        }
        return null;
    }

    public URL back() {
        if (historyIndex > 0) {
            historyIndex--;
            return history.get(historyIndex);
        }
        return null;
    }

    public void load(URL url) throws IOException {
        setPage(url);
    }

    private void displayPage(URL url) {
        try {
            load(url);
            setPageBrightness(0.3); // Set the brightness to 30% when a new page is loaded
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class StatusBar extends JPanel {

    private JLabel statusLabel;

    public StatusBar() {
        setLayout(new BorderLayout());
        statusLabel = new JLabel(" ");
        statusLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        add(statusLabel, BorderLayout.CENTER);
    }

    public void setMessage(String message) {
        statusLabel.setText(message);
    }
}

class WebToolBar extends JToolBar implements HyperlinkListener {

    private WebBrowserPane localBrowserPane;
    private JSurfer parent;
    private JButton backButton;
    private JButton forwardButton;
    private JButton reloadButton;
    private JButton homeButton;
    private JTextField urlTextField;
    private static final String ABOUT_BLANK = "blank";
    private JButton goButton;
    private JButton nightModeButton;

    public WebToolBar(WebBrowserPane browserPane, JSurfer parent) {
        super("Web Navigation");

        this.localBrowserPane = browserPane;
        this.parent = parent;
        localBrowserPane.addHyperlinkListener(this);

        urlTextField = new JTextField(25);
        urlTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                localBrowserPane.goToURL(urlTextField.getText());
            }
        });

        backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                URL url = localBrowserPane.back();
                if (url != null) {
                    urlTextField.setText(url.toString());
                }
            }
        });

        forwardButton = new JButton("Forward");
        forwardButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                URL url = localBrowserPane.forward();
                if (url != null) {
                    urlTextField.setText(url.toString());
                }
            }
        });

        reloadButton = new JButton("Reload");
        reloadButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                localBrowserPane.goToURL(urlTextField.getText());
            }
        });

        homeButton = new JButton("Home");
        homeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                localBrowserPane.goToURL(ABOUT_BLANK);
            }
        });

        goButton = new JButton("Go");
        goButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                localBrowserPane.goToURL(urlTextField.getText());
            }
        });

        nightModeButton = new JButton("Night Mode");
        nightModeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                localBrowserPane.toggleNightMode();
                // Update the UI theme for the entire application
                parent.applyDarkMode(parent.getContentPane(), localBrowserPane.nightMode);
            }
        });

        add(backButton);
        add(forwardButton);
        add(reloadButton);
        add(homeButton);
        add(urlTextField);
        add(goButton);
        add(nightModeButton);
    }

    public void hyperlinkUpdate(HyperlinkEvent event) {
        if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            urlTextField.setText(event.getURL().toString());
        }
    }
}

class ButtonTabComponent extends JPanel {
    private final JTabbedPane pane;

    public ButtonTabComponent(final JTabbedPane pane) {
        super(new BorderLayout());
        if (pane == null) {
            throw new NullPointerException("TabbedPane is null");
        }
        this.pane = pane;
        setOpaque(false);

        JLabel label = new JLabel() {
            public String getText() {
                int i = pane.indexOfTabComponent(ButtonTabComponent.this);
                if (i != -1) {
                    return pane.getTitleAt(i);
                }
                return null;
            }
        };

        add(label, BorderLayout.CENTER);
        JButton button = new JButton("x");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int i = pane.indexOfTabComponent(ButtonTabComponent.this);
                if (i != -1) {
                    pane.remove(i);
                }
            }
        });

        add(button, BorderLayout.EAST);
    }
}
