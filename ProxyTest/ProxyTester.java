import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.*;
import java.io.IOException;

public class ProxyTester extends JFrame {
    private JTextField proxyField;
    private JTextArea resultArea;

    public ProxyTester() {
        setTitle("Proxy Tester");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        proxyField = new JTextField();
        resultArea = new JTextArea();
        resultArea.setEditable(false);

        JButton testButton = new JButton("Test Proxy");
        testButton.addActionListener(new TestButtonListener());

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel("Enter Proxy (e.g., 104.233.26.60:5898)"), BorderLayout.NORTH);
        panel.add(proxyField, BorderLayout.CENTER);
        panel.add(testButton, BorderLayout.SOUTH);

        add(panel, BorderLayout.NORTH);
        add(new JScrollPane(resultArea), BorderLayout.CENTER);
    }

    private class TestButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String proxyAddress = proxyField.getText().trim();
            resultArea.setText("");
            if (!proxyAddress.isEmpty()) {
                try {
                    String[] parts = proxyAddress.split(":");
                    if (parts.length != 2) {
                        resultArea.setText("Invalid proxy format. Use IP:PORT.");
                        return;
                    }
                    String ip = parts[0];
                    int port = Integer.parseInt(parts[1]);
                    testProxy(ip, port);
                } catch (NumberFormatException ex) {
                    resultArea.setText("Invalid port number.");
                }
            } else {
                resultArea.setText("Please enter a proxy address.");
            }
        }
    }

    private void testProxy(String ip, int port) {
        resultArea.append("Testing HTTP proxy...\n");
        if (isHttpProxyWorking(ip, port, false)) {
            resultArea.append("HTTP proxy is working.\n");
        } else {
            resultArea.append("HTTP proxy is NOT working.\n");
        }

        resultArea.append("Testing HTTPS proxy...\n");
        if (isHttpProxyWorking(ip, port, true)) {
            resultArea.append("HTTPS proxy is working.\n");
        } else {
            resultArea.append("HTTPS proxy is NOT working.\n");
        }

        resultArea.append("Testing SOCKS4 proxy...\n");
        if (isSocksProxyWorking(ip, port, Proxy.Type.SOCKS, 4)) {
            resultArea.append("SOCKS4 proxy is working.\n");
        } else {
            resultArea.append("SOCKS4 proxy is NOT working.\n");
        }

        resultArea.append("Testing SOCKS5 proxy...\n");
        if (isSocksProxyWorking(ip, port, Proxy.Type.SOCKS, 5)) {
            resultArea.append("SOCKS5 proxy is working.\n");
        } else {
            resultArea.append("SOCKS5 proxy is NOT working.\n");
        }
    }

    private boolean isHttpProxyWorking(String ip, int port, boolean useHttps) {
        try {
            URL url = new URL(useHttps ? "https://binfalse.de/" : "http://checkip.dyndns.org");
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip, port));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();

                // If the response contains "Address", the proxy is working
                if (content.toString().contains("Address")) {
                    return true;
                }
            }
        } catch (IOException e) {
            // Handle the exception appropriately
        }
        return false;
    }

    private boolean isSocksProxyWorking(String ip, int port, Proxy.Type proxyType, int socksVersion) {
        try {
            SocketAddress proxyAddr = new InetSocketAddress(ip, port);
            Proxy proxy = new Proxy(proxyType, proxyAddr);
            URL url = new URL("http://checkip.dyndns.org");
            URLConnection conn = url.openConnection(proxy);
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();

            // If the response contains "Address", the proxy is working
            if (content.toString().contains("Address")) {
                return true;
            }
        } catch (IOException e) {
            // Handle the exception appropriately
        }
        return false;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            ProxyTester tester = new ProxyTester();
            tester.setVisible(true);
        });
    }
}
