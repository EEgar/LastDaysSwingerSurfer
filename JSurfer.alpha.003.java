import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import javax.swing.SwingUtilities;
import javax.swing.JLabel;
import javax.swing.BorderFactory;

public class JSurfer extends JFrame {

    private JTabbedPane tabbedPane = new JTabbedPane();
    private WebBrowserPane localBrowserPane = new WebBrowserPane();
    private StatusBar statusBar = new StatusBar();

    public JSurfer() {
        super("JSurfer Web Browser");

        JMenu fileMenu = new JMenu("File");
        fileMenu.add(new NewTabAction());
        fileMenu.addSeparator();
        fileMenu.add(new ExitAction());  // Add ExitAction here
        fileMenu.setMnemonic('F');
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);
        setJMenuBar(menuBar);

        JPanel contentPane = new JPanel(new BorderLayout());
        setContentPane(contentPane);

        contentPane.add(tabbedPane, BorderLayout.CENTER);
        contentPane.add(statusBar, BorderLayout.SOUTH);

        createNewTab();
    }

    private void createNewTab() {
        JPanel panel = new JPanel(new BorderLayout());
        WebBrowserPane webBrowserPane = new WebBrowserPane();
        localBrowserPane = webBrowserPane;

        webBrowserPane.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    statusBar.setMessage("Loading...");
                } else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
                    statusBar.setMessage(e.getURL().toString());
                } else if (e.getEventType() == HyperlinkEvent.EventType.EXITED) {
                    statusBar.setMessage(" ");
                }
            }
        });

        webBrowserPane.setDropTarget(new DropTarget(webBrowserPane, DnDConstants.ACTION_COPY, new DropTargetHandler(localBrowserPane)));

        WebToolBar toolBar = new WebToolBar(webBrowserPane);
        panel.add(toolBar, BorderLayout.NORTH);
        panel.add(new JScrollPane(webBrowserPane), BorderLayout.CENTER);
        tabbedPane.addTab("Browser " + tabbedPane.getTabCount(), panel);
    }

    private class NewTabAction extends AbstractAction {
        public NewTabAction() {
            putValue(Action.NAME, "New Browser Tab");
            putValue(Action.SHORT_DESCRIPTION, "Create New Web Browser Tab");
        }

        public void actionPerformed(ActionEvent event) {
            createNewTab();
        }
    }

    private class ExitAction extends AbstractAction {
        public ExitAction() {
            putValue(Action.NAME, "Exit");
            putValue(Action.SHORT_DESCRIPTION, "Exit Application");
        }

        public void actionPerformed(ActionEvent event) {
            System.exit(0);
        }
    }

    private class DropTargetHandler implements DropTargetListener {
        private WebBrowserPane webBrowserPane;

        public DropTargetHandler(WebBrowserPane webBrowserPane) {
            this.webBrowserPane = webBrowserPane;
        }

        public void drop(DropTargetDropEvent event) {
            Transferable transferable = event.getTransferable();
            if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                event.acceptDrop(DnDConstants.ACTION_COPY);
                try {
                    List<File> fileList = (List<File>) transferable.getTransferData(DataFlavor.javaFileListFlavor);
                    Iterator<File> iterator = fileList.iterator();
                    while (iterator.hasNext()) {
                        File file = iterator.next();
                        URL url = new URL(webBrowserPane.correctURLString(file.getName()));
                        webBrowserPane.goToURL(url);
                    }
                    event.dropComplete(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    event.dropComplete(false);
                }
            } else {
                event.rejectDrop();
            }
        }

        public void dragEnter(DropTargetDragEvent event) {
            if (event.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
                event.acceptDrag(DnDConstants.ACTION_COPY);
            else {
                event.rejectDrag();
            }
        }

        public void dragExit(DropTargetEvent event) {
        }

        public void dragOver(DropTargetDragEvent event) {
        }

        public void dropActionChanged(DropTargetDragEvent event) {
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JSurfer jsurfer = new JSurfer();
            jsurfer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jsurfer.setSize(640, 480);
            jsurfer.setVisible(true);
        });
    }
}

class WebBrowserPane extends JEditorPane {

    private List<URL> history = new ArrayList<>();
    private int historyIndex;

    public WebBrowserPane() {
        setEditable(false);
        setContentType("text/html");
    }

    public void goToURL(URL url) {
        displayPage(url);
        history.add(url);
        historyIndex = history.size() - 1;
    }

    public String correctURLString(String urlText) throws MalformedURLException {
        if (urlText == null || urlText.isEmpty()) {
            return null;
        }

        URL url;
        try {
            url = new URL(urlText);
        } catch (MalformedURLException e) {
            url = new URL("http://" + urlText);
        }

        return url.toString();
    }

    public URL forward() {
        if (historyIndex < history.size() - 1) {
            historyIndex++;
            return history.get(historyIndex);
        }
        return null;
    }

    public URL back() {
        if (historyIndex > 0) {
            historyIndex--;
            return history.get(historyIndex);
        }
        return null;
    }

    public void load(URL url) throws IOException {
        setPage(url);
    }

    private void displayPage(URL url) {
        try {
            load(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class StatusBar extends JPanel {

    private JLabel statusLabel;

    public StatusBar() {
        setLayout(new BorderLayout());
        statusLabel = new JLabel(" ");
        statusLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        add(statusLabel, BorderLayout.CENTER);
    }

    public void setMessage(String message) {
        statusLabel.setText(message);
    }
}

class WebToolBar extends JToolBar implements HyperlinkListener {

    private WebBrowserPane localBrowserPane;
    private JButton backButton;
    private JButton forwardButton;
    private JTextField urlTextField;

    public WebToolBar(WebBrowserPane browserPane) {
        super("Web Navigation");

        localBrowserPane = browserPane;
        localBrowserPane.addHyperlinkListener(this);

        urlTextField = new JTextField(25);
        urlTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                try {
                    URL url = new URL(localBrowserPane.correctURLString(urlTextField.getText()));
                    localBrowserPane.goToURL(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });

        backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                URL url = localBrowserPane.back();
                if (url != null) {
                    urlTextField.setText(url.toString());
                }
            }
        });

        forwardButton = new JButton("Forward");
        forwardButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                URL url = localBrowserPane.forward();
                if (url != null) {
                    urlTextField.setText(url.toString());
                }
            }
        });

        add(backButton);
        add(forwardButton);
        add(urlTextField);
    }

    public void hyperlinkUpdate(HyperlinkEvent event) {
        if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            URL url = event.getURL();
            localBrowserPane.goToURL(url);
            urlTextField.setText(url.toString());
        }
    }
}
